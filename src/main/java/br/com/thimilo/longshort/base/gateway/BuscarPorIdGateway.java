package br.com.thimilo.longshort.base.gateway;

import java.util.Optional;

public  interface BuscarPorIdGateway<T,ID> {
	
	Optional<T> BuscarPorId(ID id);

}
