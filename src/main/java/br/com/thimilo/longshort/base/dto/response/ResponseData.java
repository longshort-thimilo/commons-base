package br.com.thimilo.longshort.base.dto.response;

import java.util.ArrayList;
import java.util.List;

public class ResponseData {
	private List<String> warnings = new ArrayList<>();
	private List<ResponseDataErro> erros = new ArrayList<>();
	private List<String> info = new ArrayList<>();

	public void adcionarWarnings(String... mensagem) {
		for (String msg : mensagem) {
			warnings.add(msg);
		}
	}
	
	public void adcionarErros(ResponseDataErro... mensagem) {
		for (ResponseDataErro msg : mensagem) {
			erros.add(msg);
		}
	}
	
	public void adcionarInfo(String... mensagem) {
		for (String msg : mensagem) {
			info.add(msg);
		}
	}

	public List<String> getWarnings() {
		return warnings;
	}
	public List<ResponseDataErro> getErros() {
		return erros;
	}
	
	public List<String> getInfo() {
		return info;
	}
}
