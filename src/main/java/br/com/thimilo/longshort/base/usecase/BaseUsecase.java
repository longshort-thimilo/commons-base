package br.com.thimilo.longshort.base.usecase;

public interface BaseUsecase<I,O> {
	
  O executar(I input);

}
