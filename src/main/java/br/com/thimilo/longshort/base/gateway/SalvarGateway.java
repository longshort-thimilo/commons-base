package br.com.thimilo.longshort.base.gateway;


public interface SalvarGateway<T> {
	
	 T salvar(T t);

}
