package br.com.thimilo.longshort.base.dto.response;

public class ResponseDataErro {
	private  String mensagem;
	private ListaErroEnum tipo;
	
	public ResponseDataErro(String mensagem, ListaErroEnum tipo) {
		this.mensagem = mensagem;
		this.tipo = tipo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public ListaErroEnum getTipo() {
		return tipo;
	}
	
}
