package br.com.thimilo.longshort.base.dto.response;

public enum ListaErroEnum {
	CAMPOS_OBRIGATORIOS,
	DUPLICIDADE,
	ENTRADA_NAO_ENCONTRADA,
	NAO_FOI_POSSIVEL_DELETAR,
	ENTIDADE_NAO_ENCONTRADA, 
	OUTROS,
	TIMEOUT;
}