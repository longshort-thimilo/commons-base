package br.com.thimilo.longshort.base.entity;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

public abstract class BaseEntity {

	protected String id;
	
	private LocalDateTime dataHoraCriacao;
	
	

	public String getId() {
		return id;
	}

	public LocalDateTime getDataHoraCriacao() {
		return dataHoraCriacao;
	}

	public BaseEntity() {
		this.id = UUID.randomUUID().toString();
		this.dataHoraCriacao = LocalDateTime.now();
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseEntity other = (BaseEntity) obj;
		return Objects.equals(id, other.id);
	}
	
	
	
}
